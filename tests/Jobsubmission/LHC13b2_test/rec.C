void rec() {  
  AliReconstruction reco;

//
// switch off cleanESD, write ESDfriends and Alignment data
  
  reco.SetCleanESD(kFALSE);
  reco.SetWriteESDfriend();
  reco.SetWriteAlignmentData();
  reco.SetFractionFriends(.1);
//

// OADB call  
  TString ocdbpath = "local:///project/projectdirs/alice/hpc/ocdb/";
  TString rawocdb = ocdbpath + "raw/2013/OCDB/";
  TString idealocdb = ocdbpath + "Ideal/";
  TString residualocdb = ocdbpath + "Residual/";
  TString fullocdb = ocdbpath + "Full/";

// from https://savannah.cern.ch/task/?39374#comment85

  reco.SetOption("TPC","PID.OADB=TSPLINE3_MC_%s_LHC13B2_FIXn1_PASS1_PPB_MEAN"); 

// ITS Efficiency and tracking errors

  reco.SetRunPlaneEff(kTRUE);
  reco.SetUseTrackingErrorsForAlignment("ITS");

// RAW OCDB

  reco.SetDefaultStorage(rawocdb.Data());

// ITS (2 objects)

  reco.SetSpecificStorage("ITS/Align/Data",     residualocdb.Data());
  reco.SetSpecificStorage("ITS/Calib/SPDSparseDead", residualocdb.Data());

// MUON objects (1 object)

  reco.SetSpecificStorage("MUON/Align/Data", residualocdb.Data());

// TPC (7 objects)

  reco.SetSpecificStorage("TPC/Align/Data", residualocdb.Data());
  reco.SetSpecificStorage("TPC/Calib/ClusterParam", residualocdb.Data());
  reco.SetSpecificStorage("TPC/Calib/RecoParam", residualocdb.Data());
  reco.SetSpecificStorage("TPC/Calib/TimeGain", residualocdb.Data());
  reco.SetSpecificStorage("TPC/Calib/AltroConfig", residualocdb.Data());
  reco.SetSpecificStorage("TPC/Calib/TimeDrift", residualocdb.Data());
  reco.SetSpecificStorage("TPC/Calib/Correction", residualocdb.Data());

  //  reco.SetRunQA(":");
  reco.Run();
}

