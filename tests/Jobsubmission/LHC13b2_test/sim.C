void sim(Int_t nev=100) {
  AliSimulation simulator;


  //simulator.SetWriteRawData("ALL","raw.root",kFALSE);
  simulator.SetMakeSDigits("TRD TOF PHOS HMPID EMCAL MUON ZDC PMD T0 VZERO FMD");
  simulator.SetMakeDigitsFromHits("ITS TPC");
//
//

  TString ocdbpath = "local:///project/projectdirs/alice/hpc/ocdb/";
  TString rawocdb = ocdbpath + "raw/2013/OCDB/";
  TString idealocdb = ocdbpath + "Ideal/";
// RAW OCDB
  simulator.SetDefaultStorage(rawocdb.Data());

// Specific storages = 23 

//
// ITS  (1 Total)
//     Alignment from Ideal OCDB 

  simulator.SetSpecificStorage("ITS/Align/Data",  idealocdb.Data());

//
// MUON (1 object)

  simulator.SetSpecificStorage("MUON/Align/Data", idealocdb.Data()); 

//
// TPC (6 total) 

simulator.SetSpecificStorage("TPC/Calib/TimeGain",       idealocdb.Data());
simulator.SetSpecificStorage("TPC/Calib/ClusterParam",   idealocdb.Data());
simulator.SetSpecificStorage("TPC/Calib/AltroConfig",    idealocdb.Data());
simulator.SetSpecificStorage("TPC/Calib/Correction",     idealocdb.Data());
simulator.SetSpecificStorage("TPC/Align/Data",           idealocdb.Data());
simulator.SetSpecificStorage("TPC/Calib/TimeDrift",      idealocdb.Data());

//
// Vertex and magfield


  simulator.UseVertexFromCDB();
  simulator.UseMagFieldFromGRP();

  // PHOS simulation settings
  AliPHOSSimParam *simParam = AliPHOSSimParam::GetInstance();
  simParam->SetCellNonLineairyA(0.001);
  simParam->SetCellNonLineairyB(0.2);
  simParam->SetCellNonLineairyC(1.02);

//
// The rest
//
  printf("Before simulator.Run(nev);\n");
  simulator.Run(nev);
  printf("After simulator.Run(nev);\n");
}
