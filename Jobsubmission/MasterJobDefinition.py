# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
Created on 21.12.2015

@author: markusfasel
'''
from Jobsubmission.Sandbox import Sandbox

class MasterJobDefinition(object):
    
    """
    Generalised set of parameters needed to produce the master executable.
    The parameters are mainly related to the batch system and deal with queue,
    time limit ...
    
    Getters  are self-explaining
    """
    
    def __init__(self):
        """
        Constructor
        """
        self.__jobname = "default"
        self.__nparallel = 0
        self.__pythonpath = None
        self.__globalsandbox = None
        self.__cluster = "Edison"
        self.__timelimit = ""
        self.__queue = "regular"
        self.__outputlocation = ""
        
    def SetOutputLocation(self, location):
        self.__outputlocation = location
        
    def SetTimeLimit(self, limit):
        self.__timelimit = limit
    
    def GetTimeLimit(self):
        return self.__timelimit
    
    def GetQueue(self):
        return self.__queue
    
    def SetQueue(self, queue):
        self.__queue = queue
        
    def SetCluster(self, cluster):
        self.__cluster = cluster

    def SetJobname(self, name):
        self.__jobname = name

    def GetJobname(self):
        return self.__jobname

    def SetNParallel(self, n):
        self.__nparallel = n
        
    def GetNParallel(self):
        return self.__nparallel

    def SetPYTHONPATH(self, path):
        self.__pythonpath = path

    def GetPYTHONPATH(self):
        return self.__pythonpath

    def SetGlobalsandbox(self, outdir):
        self.__globalsandbox = Sandbox(outdir)
        self.__globalsandbox.Create()

    def GetGlobalSandbox(self):
        return self.__globalsandbox
    
    def GetOutputLocation(self):
        return self.__outputlocation
    
    def GetCluster(self):
        return self.__cluster
