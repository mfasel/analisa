# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
Module handling output archive creation

:since: Oct 22, 2014
:author: Markus Fasel
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory
"""

from zipfile import ZipFile
from tarfile import TarFile

class OutputArchiveFactory(object):
    
    """
    Factory class handling the creation of output files
    """
    
    def __init__(self):
        pass 
    
    @staticmethod
    def CreateOutputArchive(filename, listoffiles):
        """
        Delegating the creation of output files to child classes
        """
        if ".zip" in filename:
            return OutputArchiveZip(filename, listoffiles)
        if ".tar" in filename:
            return OutputArchiveTar(filename, listoffiles)
        return None

class OutputArchive(object):
    
    def __init__(self, filename):
        self._filename = filename
        self._status = False
        
    def GetStatus(self):
        return self._status
    
    def GetFilename(self):
        return self._filename

class OutputArchiveTar(OutputArchive):
    """
    Create tar file archvie with the list of files
    """
    
    def __init__(self, filename, listoffiles):
        """
        Constructor, does the work
        """
        OutputArchive.__init__(self, filename)
        if len(listoffiles):
            try:
                result = TarFile(filename, "w")
                for f in listoffiles:
                    result.add(f)
                result.Close()
                self._status = True
            except Exception as e:
                print "Error occurred: %s" %(str(e))        

class OutputArchiveZip(OutputArchive):
    """
    Create tar file archvie with the list of files
    """

    def __init__(self, filename, listoffiles):
        """
        Constructor, does the work
        """
        OutputArchive.__init__(self, filename)
        if len(listoffiles):
            try:
                result  = ZipFile(filename, "w")
                for f in listoffiles:
                    result.write(f)
                result.close()
                self._status = True
            except Exception as e:
                print "Error occurred: %s" %(str(e))        