# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
:since: Oct 10, 2014
:author: Markus Fasel
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory
'''

import sqlite3

class SeedpoolConnector(object):
    
    def __init__(self, location):
        """
        Constructor
        """
        self.__seedpool = Seedpool(location, False)
        
    def GetSeed(self):
        """
        Query seed
        """
        return self.__seedpool.GetNewSeed()
    
class SeedpoolCreator(object):
    """
    Object creating a seedpool
    """
    
    def __init__(self, location, numberofseeds):
        offset = 10000000
        self.__seedpool = Seedpool(location, True)
        for iseed in range(0+offset, numberofseeds+offset+1):
            self.__seedpool.InsertIntoPool(iseed)
        
class Seedpool(object):
    """
    Class handling monte carlo seeds
    """

    def __init__(self, datalocation, doCreate = False):
        """
        Constructor
        """
        self.__db = sqlite3.connect(datalocation,50)
        self.__docreate = doCreate
        if doCreate:
            cursor = self.__db.cursor()
            cursor.execute("CREATE TABLE Seeds (value INT, isUsed INT)")
            
    def __del__(self):
        """
        destructor, closing connection to the database
        """
        self.__db.close()
    
    def InsertIntoPool(self, value):
        """
        Insert a new seed into the seedpool
        """
        if not self.__docreate:
            print "Process not allowed to insert new values into the seed pool"
            return
        cursor = self.__db.cursor()
        cursor.execute("INSERT INTO Seeds VALUES(?, ?)", (value, 0))
        self.__db.commit()
        
    def GetNewSeed(self):
        """
        Retrieve seed from the seedpool
        """
        cursor = self.__db.cursor()
        cursor.execute("SELECT * from Seeds WHERE isUsed = 0")
        nextentry = cursor.fetchone()[0]
        # Mark the seed as used
        cursor.execute("UPDATE Seeds SET isUsed=1 WHERE value=%d" %(nextentry))
        self.__db.commit()
        return nextentry
