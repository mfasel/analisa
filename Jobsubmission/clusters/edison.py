# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
Created on 21.12.2015

@author: markusfasel
'''

import subprocess, os 
from Jobsubmission.Cluster import Cluster

class edison(Cluster):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        Cluster.__init__(self)
        self._name = "Edison"
        self._sandboxLocation = os.path.join(os.getenv("SCRATCH"), "analisa")
        self._defaultQueue = "regular"
        self._defaultTime = "12:00:00"
        self._corespernode = 24
        
    def GenerateMasterScript(self, jobdefinition, workerconfig):
        output = open("%s/master.sh" %(jobdefinition.GetGlobalSandbox().GetLocation()), "w")
        output.write("#! /bin/bash -l\n")
        output.write("#SBATCH -J %s\n" %(jobdefinition.GetJobname()))
        output.write("#SBATCH -p %s\n" %(jobdefinition.GetQueue()))
        nnodes = int(jobdefinition.GetNParallel()/self._corespernode)
        if nnodes < 1:
            nnodes = 1
        output.write("#SBATCH -N %d\n" %nnodes)
        output.write("#SBATCH -t %s\n" %jobdefinition.GetTimeLimit())

        output.write("#SBATCH --error %s/%s_%%j.err\n" %(jobdefinition.GetOutputLocation(), jobdefinition.GetJobname()))
        output.write("#SBATCH --output %s/%s_%%j.out\n" %(jobdefinition.GetOutputLocation(), jobdefinition.GetJobname()))
        
        basemodules = ["/opt/cray/ari/modulefiles","/opt/cray/craype/2.4.2/modulefiles","/opt/cray/modulefiles",\
                        "/opt/modulefiles","/usr/common/software/modulefiles","/usr/syscom/nsg/modulefiles",\
                        "/usr/syscom/nsg/opt/modulefiles","/usr/common/ftg/modulefiles","/opt/cray/ari/modulefiles",\
                        "/opt/cray/craype/2.4.2/modulefiles","/opt/cray/modulefiles","/opt/modulefiles","/usr/common/software/modulefiles",\
                        "/usr/syscom/nsg/modulefiles","/usr/syscom/nsg/opt/modulefiles","/usr/common/ftg/modulefiles"]
        mainmodules=["python", "mpi4py"]
        for modpath in basemodules:
            output.write("module use %s\n" %(modpath))
        
        # export jobid on jobscript level
        output.write("export CLUSTER_JOBID=$SLURM_JOBID\n")
        output.write("export CRAY_ROOTFS=DSL\n")
        for mymod in mainmodules:
            output.write("module load %s\n" %(mymod))
        #set the pythonpath:
        output.write("export PYTHONPATH=$PYTHONPATH:%s\n" %jobdefinition.GetPYTHONPATH())
        output.write("srun -n %d python-mpi %s/Jobsubmission/Worker.py %s %s\n" %(int(jobdefinition.GetNParallel()), jobdefinition.GetPYTHONPATH(), workerconfig["mode"], workerconfig["param"]))


    def SubmitMasterJob(self, sandbox):
        executable = "sbatch %s/master.sh" %sandbox
        if self._debugMode:
            print "Here I would do \' %s\'" %(executable) 
        else:
            submitproc = subprocess.Popen(executable, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) 
            for line in submitproc.stdout:
                self._jobid = int(line.rstrip().split(" ")[3])
                submitproc.stdout.flush()
                print "Job successfully submitted with ID %d" %(self._jobid)
                
