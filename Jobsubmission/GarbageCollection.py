#! /usr/bin/env python
# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
:since: Jun 8, 2015
:author: Markus Fasel
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory
'''

import getopt, getpass, os, shutil, subprocess, sys, time

if __name__ == "__main__":
    # Add module to the PYTHONPATH
    mymodules =  os.path.dirname(sys.argv[0])
    hasFound = False
    for ipath in sys.path:
        if ipath == mymodules:
            hasFound = True
    if not hasFound:
        sys.path.append(mymodules)
    
from Jobsubmission.HPCJobInfo import HPCJobHandler

def GetListOfJobs(jobidfile):
    result = []
    with open(jobidfile) as reader:
        for line in reader:
            if not len(line):
                continue
            result.append(int(line))
        reader.close()
    return result

def GarbageCollection(jobidinfo, sandbox):
    jobhandler = HPCJobHandler()
    listofuserjobs = GetListOfJobs(str(jobidinfo)) if isinstance(jobidinfo, str) else [jobidinfo] 
    nnonfinished = 1
    while nnonfinished:
        # Determine number of non-finished jobs
        nnonfinished = 0
        joblist = jobhandler.GetJobListForUser(getpass.getuser())
        for jobid in listofuserjobs:
            edijob = joblist.FindJob(jobid)
            if edijob and not edijob.GetJobStatus() == "C":
                # job in state running 
                nnonfinished += 1
        if nnonfinished:
            time.sleep(600)
    shutil.rmtree(sandbox)
    
if __name__ == '__main__':
    sandbox = ""
    jobid = -1
    opt, arg = getopt.getopt(sys.argv[1:], "j:w:f:", ["jobid=", "file=", "sandbox=", "start", "run"])
    startDaemon=False
    runDaemon=False
    jobidfile = None
    for o,a in opt:
        if o in ["-j", "--jobid"]:
            jobid = int(a)
        elif o in ["-w", "--sandbox"]:
            sandbox = str(a)
        elif o in ["-f", "--file"]:
            jobidfile = str(a)
        elif o in ["--start"]:
            startDaemon = True
        elif o in ["--run"]:
            runDaemon = True
    if not len(sandbox):
        sys.exit(2)
    if jobid < 0 and not jobidfile:
        sys.exit(2)
    if startDaemon: 
        print "Starting Garbage collection Daemon"
        args = [sys.argv[0], "--run", "--sandbox=%s" %sandbox]
        if jobid >= 0:
            args.append("--jobid=%s" %jobid)
        elif jobidfile:
            args.append("--file=%s" %jobidfile)
        subprocess.Popen(args)
    if runDaemon:
        GarbageCollection(jobidfile if jobidfile else jobid, sandbox) 
