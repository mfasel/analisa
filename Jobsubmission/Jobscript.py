# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
Created on 21.12.2015

@author: markusfasel
'''
import os

class Jobscript(object):
    """
    Implementation of the job executable with special cluster and user configurations
    """
    
    def __init__(self, jobparams, location, jobscriptname = "jobscript.sh"):
        """
        Constructor initalising values
        """
        self.__jobparams = jobparams
        self.__jobscriptname = jobscriptname
        self.__location = location
        
    def Create(self):
        """
        Create the jobscript
        """
        jobscript = open("%s/%s" %(self.__location, self.__jobscriptname), "w")
        jobscript.write("#!/bin/bash\n")
        jobscript.write("WD=$1\n")
        jobscript.write("JOBID=$2\n")
        jobscript.write("cd $WD\n")
        # Copy all macros to the sandbox
        # Add own module paths
        for modpath in self.__jobparams.GetUserModulePaths():
            jobscript.write("module use %s\n" %(modpath))
        # Load own modules
        for mymod in self.__jobparams.GetUserModules():
            jobscript.write("module load %s\n" %(mymod))
        jobscript.write("env\n");
        jobscript.write("module list\n")
        jobscript.write("cd $WD\n")
        jobscript.write("%s %s\n" %(self.__jobparams.GetExecutable(), self.__jobparams.GetExecutableArgs().replace("\"","").replace("#jobid", "$JOBID")))
        jobscript.close()
        os.chmod("%s/%s" %(self.__location, self.__jobscriptname), 0755)
        
    def Execute(self, sandbox, jobid):
        if sandbox.IsCreated():
            os.system("%s/%s %s %d &> %s/worker.log" %(self.__location, self.__jobscriptname, sandbox.GetLocation(), jobid, sandbox.GetLocation()))
            
    def MakeDict(self):
        return {"jobparams":self.__jobparams, "scriptname":self.__jobscriptname, "location":self.__location}
    
    def FromDict(self, mydict):
        """
        Reconstruct object from dictionary
        """
        self.__jobparams = mydict["jobparams"]
        self.__jobscriptname = mydict["scriptname"]
        self.__location = mydict["location"]
    
    def __getstate__(self):
        return self.MakeDict()

    def __setstate__(self, mydict):
        self.FromDict(mydict)
    
        